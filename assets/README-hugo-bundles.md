# My experiences with Hugo’s template lookup order

I just want to share some of my experiences by testing the lookup order for templates. I tested these and wrote them down the same time. Nevertheless I could be wrong in some cases. So if you have better solutions or remarks please don't hesitate to share.

In this example we work with a sample directory `hugo-docs` and different layout variants.

For this purpose we create this directory (`content/hugo-docs`) and also create an `index.md` (note the spelling!) in this directory. This `index.md` will later contain the content for the page `domain.tld/hugo-docs/`.

If an additional `_index.md` is created in the directory, it is used as a list page for the directory contents. You can assign an individual list template to it by creating `layouts/hugo-docs/list.html`. This template is then automatically taken by Hugo because the directory names are identical (`content/hugo-docs` and `layouts/hugo-docs`).

## Layout variants

### Variant 1

By default, Hugo takes the first layout `single.html` found. This is usually in `layouts/_default/single.html`.

### Variant 2

An alternative layout can be used by creating a directory in `layouts` for this page (e. g. `layouts/hugo-docs`) and creating a `single.html` in this directory. In this case, Hugo automatically takes this layout because its directory has the same name as the page itself.

### Variant 3

If a new template `layouts/hugo-docs/test.html` is now created in this directory, this can be selected in the Front Matter of the page with

~~~html
layout = "test"
~~~

### Variant 4

Another option is to select a layout from a different layouts directory. To do this, specify the type of the layout in the Front Matter with the directory name:

~~~html
type = "static page"
~~~

Now Hugo automatically takes the template `single.html` from `layouts/static-page`.

### Variant 5

If you create another template `test.html` in `layouts/static-page`, you can select this in the Front Matter with

~~~html
type   = "static page"
layout = "test"
~~~

### Variant 6

You can also create a layout in `layouts/_default` with the name of the page `hugo-docs.html`. Either Hugo will take this automatically if no more suitable layout is found in the lookup order, or it will be selected in the Front Matter of the page with

~~~html
layout = "hugo-docs"
~~~

In the directory `hugotest-master` of bep there are examples for bundles e. g. in the directory `hugotest-master/content/section3/myarticle`.

In the directory `hugotest-master` of @bep Hugo Demo project are examples for the new bundles feature e. g. in the directory `hugotest-master/content/section3/myarticle`. The project can be found here: [Bep Hugotest](http://hugotest.bep.is/)
