#!/usr/bin/env bash

# You’d run this in the same directory that has the content files you want edited. What it does:
#
# - Loops through all .md files in the directory
# - Sets a weight var with the first 2 chars of the filename
# - Calls an awk program that inserts the weight: xx into the front matter, right above the last '---'
# - It then overwrites the file with the new changes
#
# This assumes yaml for your front matter. If you use toml, you’ll need to adjust this script accordingly.

for file in *.md; do
  weight=${file:0:2}
  awk -v weight=$weight '/---/{
    count++
    if(count == 2){
    sub("---","weight: " weight "\n---",$0)
    }
  }
  {print}' $file > tmp && mv tmp $file
done

# Then you should be able to do
#
# {{ range .Pages.ByWeight }}
#   <!-- some code -->
# {{ end
