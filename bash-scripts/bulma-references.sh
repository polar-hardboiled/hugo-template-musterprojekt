#!/usr/bin/env bash

echo
echo "-------------------------------------------------------------------------------"
echo "This script copies Bulma's Initial and Derived Variables to"
echo "'./assets/scss/bulma-reference'."
echo "-------------------------------------------------------------------------------"
echo "Copy in progress..."
echo

# bulma files to './assets/scss' as references
if [ ! -d "assets/scss/bulma-reference" ]
then
    mkdir assets/scss/bulma-reference

        cp -f ./assets/dependencies/bulma/sass/utilities/initial-variables.sass ./assets/scss/bulma-reference/initial-variables.scss
        cp -f ./assets/dependencies/bulma/sass/utilities/derived-variables.sass ./assets/scss/bulma-reference/derived-variables.scss
fi

# Search ' !default' in bulma references and replace with semicolon (replace string would go between the 2 slashes)
sed -i.bak 's/ !default/;/' assets/scss/bulma-reference/derived-variables.scss
sed -i.bak 's/ !default/;/' assets/scss/bulma-reference/initial-variables.scss
rm -f assets/scss/bulma-reference/derived-variables.scss.bak
rm -f assets/scss/bulma-reference/initial-variables.scss.bak

echo
echo "...ready."
echo "-------------------------------------------------------------------------------"
echo
exit 0
