#--------------------------------------------------------------------------------------------------
# General settings
#--------------------------------------------------------------------------------------------------
title                  = "Hugo Example Project"
baseURL                = ""
languageCode           = "en"
DefaultContentLanguage = "en"
canonifyURLs           = true
pluralizeListTitles    = false

# See https://gohugo.io/templates/menu-templates/#section-menu-for-lazy-bloggers
# and https://gohugo.io/templates/menu-templates/#menu-entries-from-the-page-s-front-matter
# SectionPagesMenu       = "main"

enableRobotsTXT        = false
verbose                = false
newContentEditor       = "Code"
# enableEmoji            = true

#--------------------------------------------------------------------------------------------------
# Google Analytics is set in './layouts/partials/_default/baseof.html'
#--------------------------------------------------------------------------------------------------
# The internal async template is used
googleAnalytics = ""

#--------------------------------------------------------------------------------------------------
# Author
#--------------------------------------------------------------------------------------------------
author = ["John Doe"]

#--------------------------------------------------------------------------------------------------
# Taxonomies
#--------------------------------------------------------------------------------------------------
# The entries 'tag' and 'category' are default and don't need to be set here.
# In case you have custom taxonomies (e.g. series) they have to be set!
#
# To disable taxonomies:
# disableKinds = ["taxonomy", "taxonomyTerm"]
#
# https://www.netlify.com/blog/2018/07/24/hugo-tips-how-to-create-author-pages/?utm_campaign=tool&utm_medium=website&utm_source=thenewdynamic.org
# disableKinds = ["taxonomyTerm"]
[taxonomies]
  tag      = "tags"
  category = "categories"
  series   = "series"
  # https://www.netlify.com/blog/2018/07/24/hugo-tips-how-to-create-author-pages/?utm_campaign=tool&utm_medium=website&utm_source=thenewdynamic.org
  author   = "authors"

#--------------------------------------------------------------------------------------------------
# Permalinks
#--------------------------------------------------------------------------------------------------
# Since Hugo 0.22.1
# http://gohugo.io/news/0.22.1-relnotes/
# https://gohugo.io/content-management/urls/#permalinks-configuration-example
#
# # First level only:
# [permalinks]
#   blog = ":section/:title"

# # Nested (all levels):
# [permalinks]
#   blog = ":sections/:title"

# # Permalinks can be set for all other sections:
# [permalinks]
#   post = "/:year/:month/:title/"
#   code = "/:filename/"

#--------------------------------------------------------------------------------------------------
# Blackfriday
# https://gohugo.io/getting-started/configuration/#configure-blackfriday
#--------------------------------------------------------------------------------------------------
[blackfriday]
  angledQuotes = true

#--------------------------------------------------------------------------------------------------
# Sitemap
#--------------------------------------------------------------------------------------------------
[sitemap]
  changefreq = "monthly"
  priority   = 0.5
  filename   = "sitemap.xml"

#--------------------------------------------------------------------------------------------------
# General Data Protection regulations (GDPR)
# Germany: Datenschutzgrundverordnung (DSGVO)
#--------------------------------------------------------------------------------------------------
# https://gohugo.io/about/hugo-and-gdpr/#the-privacy-settings-explained
#
[privacy]
  [privacy.disqus]
    disable           = false
  [privacy.googleAnalytics]
    anonymizeIP       = true
    disable           = false
    respectDoNotTrack = true
    useSessionStorage = true
  [privacy.instagram]
    disable           = false
    simple            = false
  [privacy.speakerDeck]
    disable           = false
  [privacy.twitter]
    disable           = false
    enableDNT         = false
  [privacy.vimeo]
    disable           = false
  [privacy.youtube]
    disable           = false
    privacyEnhanced   = false

#--------------------------------------------------------------------------------------------------
# Params
#--------------------------------------------------------------------------------------------------
[params]

  # Debug CSS
  # See https://gohugo.io/hugo-pipes/resource-from-string/
  # and https://scrimba.com/p/pV5eHk/c9EqQSD
  # Put a value like "true" or "yes" (doesn't matter at all)
  # in 'debugCSS' and your site will show a debug grid.
  debugCSS = ""

  # Custom Colors
  # $backgroundColor and $textColor are defined in './assets/scss/main.scss'.
  # In './layouts/partials/css.html' they're executed as template.
  fontColor = ""
  linkColor = ""

  # Environment Development
  env = "DEVELOPMENT"

  # Sidebar
  SidebarRecentLimit = 5

  # Date Format
  dateFormat = "2. Januar 2006"

  # Short description of the page (limit to 150 characters)
  description = "Diese Beschreibung steht in der config.toml"

  # Keywords
  keywords = ["Keyword1", "Keyword2", "Keyword3", "Keyword4"]

  # SEO Image
  images = "images/seo-image.jpg"

  # Images URL
  imageURL = "/images/"

  # Owner
  [params.owner]
    company        = ""
    name           = "Hans Mustermann"
    email          = "hans@mustermann.de"
    strasse        = ""
    plz            = ""
    ort            = ""
    mobile         = ""
    fon            = ""
    homepageTitle  = "Hans Mustermann - Berufsbezeichnung"
    url            = "https://mustermann.de"

  # Google Fonts
  # Google Fonts are set in './assets/scss/abstracts/variables.scss' as '@import'.
  # In the same file also the variables for fonts are set. They are used in
  # './assets/scss/base/typography.scss' and can be deactivated as well (just comment them)
  # which will cause the standard fonts to be rendered.

  # SEO
  # http://brendan-quinn.xyz/post/working-with-hugos-internal-partial-templates-facebook-and-open-graph/

  # Webmaster Tools Verification
  [params.webmaster_verifications]
    google = ""
    bing   = ""
    alexa  = ""

  # Social Network
  [params.social]
    twitter         = "Hans2000"
    facebook        = ""
    facebook_admin  = ""
    facebook_app_id = "<!-- Get from the FB Dev Page -->"

  # Map Details
  # Data are processed in './layouts/partials/map/mapContainer.html'
  [params.geo]
    title        = "Here you can find me" # Custom title
    width        = "100%" # Pixel value as well possible
    height       = 300
    pre_embed    = "https://www.openstreetmap.org/export/embed.html?"
    embed        = "bbox=9.709746837615969%2C52.36640889758257%2C9.722986221313477%2C52.37461015077904&amp;" # Copy this value from OpenStreetMap
    map_big      = "mlat=52.3664&amp;mlon=9.7229#map=16/52.3664/9.7229" # Copy this value from OpenStreetMap
    map_big_text = "Größere Karte bitte" # Custom text

  # CDN Usage
  [params.cdn]
    # See https://www.srihash.org for info on how to generate the hashes
    css         = ""
    css_hash    = ""
    js          = ""
    js_hash     = ""
    jquery      = ""
    jquery_hash = ""
    popper      = ""
    popper_hash = ""

#--------------------------------------------------------------------------------------------------
# Menus
#
#   The menus in 'config.toml' are deactivated because they come from pages Front Matter
#   See on top of this document and https://gohugo.io/templates/menu-templates/#menu-entries-from-the-page-s-front-matter
#
#   Also see: https://gohugo.io/templates/menu-templates/#site-config-menus
#--------------------------------------------------------------------------------------------------
# See 'SectionPagesMenu' above and the if/or condition in './layouts/partials/footer-header/header.html'
[[menu.main]]
name = "Tags"
weight = 100
url = "/tags/"
[[menu.main]]
name = "Categories"
weight = 110
url = "/categories/"
[[menu.main]]
name = "Series"
weight = 120
url = "/series/"
#
# [[menu.main]]
# name   = "Documentation"
# weight = 90
# url    = "/documentation/index.html"

# [[menu.main]]
# name   = "Posts"
# pre    = "<i class=\"fa fa-question-circle\" aria-hidden=\"true\"></i>"
# weight = 5
# url    = "/posts/"

# [[menu.footer]]
# name   = "Imprint"
# weight = 5
# url    = "/imprint/"

# [[menu.footer]]
# name   = "Contact"
# weight = 10
# url    = "/contact/"
