+++
title       = "Imprint"
date        = 2017-07-14T15:40:34+01:00
description = "This is the imprint and very important"

bodyclass   = "imprint"

[menu.footer]
weight = 10
+++

{{< divider >}}

[//]: # (Here's a long markdown comment which is not rendered in HTML.)

This is the content with the shortcode `humanSitemap.html`!

{{< humanSitemap >}}
