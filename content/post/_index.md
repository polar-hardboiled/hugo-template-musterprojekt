+++
title = "The Posts"
showPages = "all"

[menu.main]
name   = "Posts"
weight = 50
+++

This is `content/post/_index.md`.

It's title is "The Posts" and appears in the main menu as "Posts" with a weight of 50.
